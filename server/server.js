require('./config/config');
const {ObjectId} = require('mongodb');
const express = require('express');
const _ = require('lodash');
const bodyParser = require('body-parser');

var {User} = require( './models/user');
var {mongoose} = require('./db/mongoose');
var {authenticate} = require('./middleware/authenticate');

var app = express();
const port = 3000;

app.use(bodyParser.json());

app.post('/users', (req, res) => {
  var body = _.pick(req.body, ['email', 'password', 'dateOfBirth', 'number', 'username']);
  var user = new User(body);

  user.save().then(() => {
    return user.generateAuthToken();
  }).then((token) => {
    res.header('x-auth', token).send(user);
  }).catch((e) => {
    res.status(400).send(e);
  });
});

app.get('/users/me', authenticate, (req, res) => {
  res.send(req.user);
});

app.listen(port, () => {
  console.log(`Starting on port ${port}`);
});

module.exports = {app};
